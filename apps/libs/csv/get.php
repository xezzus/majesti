<?php
return function(){
  $file = __DIR__.'/../../../data.csv';
  if(!is_file($file)) return false;
  $csv = file($file,FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
  array_shift($csv);
  foreach($csv as $key=>$value){
    $csv[$key] = str_getcsv($value,';');
  }
  return $csv;
}
?>
