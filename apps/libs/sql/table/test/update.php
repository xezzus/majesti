<?php
return function($name,$status){
  $db = $this->db->pg();
  $sql = "update test set status = :status where name = :name returning true as true;";
  $sql = $db->prepare($sql);
  $sql->execute([':status'=>$status,':name'=>$name]);
  $res = $sql->fetch();
  if($res === false) return false;
  else return true;
}
?>
