<?php
return [function(){
  if(!$this->sql->table->test->is()) {
    $this->sql->table->test->create();
    $csv = $this->csv->get();
    foreach($csv as $value){
      $this->sql->table->test->insert($value[0],$value[1]);
    }
  }
  $db = $this->db->pg();
  $sql = "select count(0) from test";
  $count = $db->query($sql)->fetch()['count'];
  $offset = mt_rand(0,$count-1);
  $sql = "select name,status from test offset $offset limit 1";
  $res = $db->query($sql)->fetch();
  if($res['status'] == 0) $status = 1;
  else $status = 0;
  $this->sql->table->test->update($res['name'],$status);
  return ['name'=>$res['name'],'status'=>$status];
},'PUBLIC']
?>
