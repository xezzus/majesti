<?php
return function(){
  $db = $this->db->pg();
  $sql = "CREATE TABLE test (
    name        text PRIMARY KEY,
    status      smallint not null
  );";
  $res = $db->exec($sql);
  return true;
}
?>
